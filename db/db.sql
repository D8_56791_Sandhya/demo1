CREATE TABLE product(id INT PRIMARY KEY,title VARCHAR(100),price FLOAT);

INSERT INTO product VALUES(1,"Mobile",15000),(2,"TV",50000),(3,"Speaker",500);
