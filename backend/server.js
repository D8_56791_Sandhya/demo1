const express=require('express')
const db=require("./db")
const app=express()

app.use(express.json())

app.get("/",(request,response)=>{

    const connection=db.openConnetion()

    const sql=`SELECT * FROM product`

    connection.query(sql,(error,result)=>{
        connection.end()
        response.send(result)


    })
    
})

app.delete("/:id",(request,response)=>{
    const{id}=request.params
    const connection=db.openConnetion()

    const sql=`DELETE FROM product WHERE id=${id}`

    connection.query(sql,(error,result)=>{
        connection.end()
        response.send(result)

    })
    
})

app.put("/:id",(request,response)=>{
    const{ id }=request.params
    const{ title , price }=request.body
    const connection=db.openConnetion()

    const sql=`UPDATE product SET title='${title}',price=${price} WHERE id=${id}`
    connection.query(sql,(error,result)=>{
        connection.end()
        response.send(result)

    })
    
})

app.post("/add",(request,response)=>{
    const{ id, title, price}=request.body
    const connection=db.openConnetion()

    const sql=`INSERT INTO product VALUES(${id},'${title}',${price})`

    connection.query(sql,(error,result)=>{
        connection.end()
        response.send(result)

    })
    
})

app.listen(4000,()=>{
    console.log('server started on port 4000')
})